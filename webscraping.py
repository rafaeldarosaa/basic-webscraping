import time
import requests
import pandas as pd
from selenium import webdriver
from selenium.webdriver.firefox.options import Options
from bs4 import BeautifulSoup
import json

# web page that will be crawled
url = "https://www.mercadolivre.com.br/ofertas?domain_id=MLB-NOTEBOOKS#origin=scut&filter_applied=domain_id&filter_position=4&is_recommended_domain=true"

option = Options()
option.headless = False

driver = webdriver.Firefox(options=option)

driver.get(url)

# sleep to see what's going on
time.sleep(3)

#click on cookie button
driver.find_element_by_xpath("//button[@class='nav-cookie-disclaimer__button']").click()

#products 
price_el = driver.find_element_by_xpath("//ol[@class='items_container']")
products_container = price_el.get_attribute("outerHTML")

#html parse
parsed_html = BeautifulSoup(products_container, 'html.parser')
products_list = parsed_html.find_all("li", class_="promotion-item")

for product in products_list:
    title = product.find("p", class_="promotion-item__title").contents[0]
    price = product.find("span", class_="promotion-item__price").find("span").contents[0]
    discount = product.find("span", class_="promotion-item__discount").contents[0]

print(title)
print(price)
print(discount)

driver.quit()